
//
// This is example code from Chapter 6.7 "Trying the second version" of
// "Software - Principles and Practice using C++" by Bjarne Stroustrup
//

 
/*
    Simple calculator

    Revision history:
        
        Revised by Bjarne Stroustrup May 2007
        Revised by Bjarne Stroustrup August 2006
        Revised by Bjarne Stroustrup August 2004
        Originally written by Bjarne Stroustrup
            (bs@cs.tamu.edu) Spring 2004.
    
    This program implements a basic expression calculator.
    Input from cin; output to cout.

    The grammer for input is:

    Calculation:
        Statement
        Print
        Quit
        Calculation Statement

    Statement:
        Declaration
        Expression

    Declaration:
        "let" Name "=" Expression

    Name:
        alphabet
        alphabet non-space-characters
        _ Name
    
    Print:
        ;
    
    Quit:
        q

    Expression:
        Term
        Expression + Term
        Expression - Term
    
    Term:
        Primary
        Term * Primary
        Term / Primary
        Term % Primary

    Primary:
        Number
        ( Expression )
        - Primary
        + Primary
        Name = Expression
    
    Number:
        floating-point-literal

    Input comes from cin through the Token_stream called ts.
*/

#include "../std_lib_facilities.h"
#include "token.h"

class Variable {
public:
    string name;
    double value;
    Variable (string n, double v) :name(n), value(v) { }
};

//------------------------------------------------------------------------------

const char number = '8';
const char quit = 'q';
const char print = ';';
const char name = 'a';
const char let = 'L';
const string prompt = "> ";
const string result = "= ";
const string declkey = "let";
//------------------------------------------------------------------------------

bool isunderscore(char c)
{
    return c == '_';

vector<Variable> var_table;

double get_value(string s)
{
    for (int i = 0; i < var_table.size(); ++i)
        if (var_table[i].name == s) return var_table[i].value;
    error("get: undefined variable ", s);
}

void set_value(string s, double d)
{
    for (int i = 0; i < var_table.size(); ++i)
        if (var_table[i].name == s) {
            var_table[i].value = d;
            return;
        }
    error("set: undefined variable ", s);
}

bool is_declared(string var)
{
    for (int i = 0; i < var_table.size(); ++i)
        if (var_table[i].name == var) return true;
    return false;
}

double define_name(string var, double val)
{
    if (is_declared(var)) error(var, " declared twice");
    var_table.push_back(Variable(var, val));
    return val;
}

double substitute(string var, double val)
{
    cout << "substitute\n";
    for (int i = 0; i < var_table.size(); ++i) {
        if (var_table[i].name == var) {
            var_table[i].value = val;
            return val;
        }
    }
    error(var, " not declared");
}

//------------------------------------------------------------------------------


// kaijyo keisan
double calculateFactorial(int n)
{
    if (n < 0) error("can not calculate factorial.");
    if (n == 0) return 1;

    return n * calculateFactorial(n - 1);
}

//------------------------------------------------------------------------------

Token_stream ts;        // provides get() and putback() 

//------------------------------------------------------------------------------

double expression();    // declaration so that primary() can call expression()

//------------------------------------------------------------------------------

double declaration()
{
    Token t = ts.get();
    if (t.kind != name) error ("name expected in declaration");
    string var_name = t.name;

    Token t2 = ts.get();
    if (t2.kind != '=') error ("= missing in declaration of ", var_name);

    double d = expression();
    define_name(var_name, d);
    return d;
}

double substitution(string s)
{
    double d = expression();
    substitute(s, d);
    return d;
}

//------------------------------------------------------------------------------

double statement()
{
    Token t = ts.get();
    switch (t.kind) {
        case let:
            return declaration();
        default:
            ts.putback(t);
            return expression();
    }
}

//------------------------------------------------------------------------------

// deal with numbers and parentheses
double primary()
{
    Token t = ts.get();
    switch (t.kind) {
    case '(':    // handle '(' expression ')'
    {    
        double d = expression();
        t = ts.get();
        if (t.kind != ')') error("')' expected");
        return d;
    }
    case number:
        return t.value;  // return the number's value
    case '-':
        return - primary();
    case '+':
        return primary();
    case name:
        {
            Token t2 = ts.get();
            if (t2.kind == '=') {
                double d = expression();
                set_value(t.name, d);
                return d;
            } else {
                ts.putback(t2);
                return get_value(t.name);
            }
        }
    default:
        cout << t.kind << endl;
        error("primary expected");
    }
}

//------------------------------------------------------------------------------

// deal with !
double factorial()
{
    double left = primary();
    Token t = ts.get();  // get the next token from token stream.

    switch (t.kind) {
    case '!':
        {
            int n = left;
            return calculateFactorial(n);
        }
        break;
    default:
        ts.putback(t);  // put t back into the token stream.
        return left;
    }
}

//------------------------------------------------------------------------------

// deal with *, /, and %
double term()
{
    double left = factorial();
    Token t = ts.get();        // get the next token from token stream

    while(true) {
        switch (t.kind) {
        case '*':
            left *=factorial();
            t = ts.get();
            break;
        case '/':
            {    
                double d = factorial();
                if (d == 0) error("divide by zero");
                left /= d; 
                t = ts.get();
                break;
            }
        case '%':
            {
                double d = primary();
                int i1 = int(left);
                if (i1 != left)
                        error("left-hand operand of % not int");
                int i2 = int(d);
                if (i2 != d)
                        error("right-hand operand of % not int");
                if (i2 == 0)
                        error("%: divide by zero");
                left = i1%i2;
                t = ts.get();
                break;
            }
        default: 
            ts.putback(t);     // put t back into the token stream
            return left;
        }
    }
}

//------------------------------------------------------------------------------

// deal with + and -
double expression()
{
    double left = term();      // read and evaluate a Term
    Token t = ts.get();        // get the next token from token stream

    while(true) {    
        switch(t.kind) {
        case '+':
            left += term();    // evaluate Term and add
            t = ts.get();
            break;
        case '-':
            left -= term();    // evaluate Term and subtract
            t = ts.get();
            break;
        default: 
            ts.putback(t);     // put t back into the token stream
            return left;       // finally: no more + or -: return the answer
        }
    }
}

void clean_up_mess()
{
    ts.ignore(print);
}

void calculate()
{
    while (cin) {
        try {
            cout << prompt;
            Token t = ts.get();
            while (t.kind == print) t = ts.get(); // ;を飲み込む
            if (t.kind == quit) return;
            ts.putback(t);
            cout << result << statement() << '\n';
        }
        catch (exception& e) {
            cerr << e.what() << endl;
            clean_up_mess();
        }
    }
}

//------------------------------------------------------------------------------

int main()
try
{
    define_name("pi", 3.1415926535);
    define_name("e", 2.7182818284);

    calculate();
    
    keep_window_open(); // for Windows console mode 
    return 0;
}
catch (exception& e) {
    cerr << e.what() << '\n'; 
    keep_window_open("~~");
    return 1;
}
catch (...) {
    cerr << "exception \n"; 
    keep_window_open("~~");
    return 2;
}

//------------------------------------------------------------------------------
