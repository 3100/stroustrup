#include <iostream>

double ctok(double c)
{
	//int k = c + 273.15;
	const double min = -273.15;
	if (c < min)
	{
		error("input a double which is bigger than -273.15 ");
	}
	double k = 9.0 * c / 5.0 + 32;
	return k;
}

void error(std::string message)
{
	std::cerr << message << std::endl;
	exit(1);
}

int main()
{
	double c = 0;
	std::cin >> c;
	if (!std::cin)
	{
		error("couldn't read a double "); 
	}
	double k = ctok(c);
	std::cout << k << std::endl;
}
