#include <iostream>

double ctok(double c);
double ktoc(double k);
void error(std::string message);

double ctok(double c)
{
	const double min = -273.15;
	if (c < min)
	{
		error("input a double which is bigger than -273.15 in 'ctok' ");
	}
	double k = 9.0 * c / 5.0 + 32;
	return k;
}

double ktoc(double k)
{
	const double min = 0;
	if (k < min)
	{
		error("input a double which is bigger than 0 in 'ktoc' ");
	}
	double c = (k - 32) * 5.0 / 9.0;
	return c;
}

void error(std::string message)
{
	std::cerr << message << std::endl;
	exit(1);
}

int main()
{
	std::cout << ""
	double c = 0;
	std::cin >> c;
	if (!std::cin)
	{
		error("couldn't read a double "); 
	}
	double k = ctok(c);
	std::cout << k << std::endl;
}
