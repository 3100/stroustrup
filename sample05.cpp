#include <iostream>
#include <string>
#include <stdexcept>

void error(std::string message);
void error(std::string message1, std::string message2);
double some_function();

double some_function()
{
	double d = 0;
	std::cin >> d;
	if (!std::cin)
	{
		error("couldn't read a double in 'some_function()'");
	}
	return d;
}

void error(std::string message)
{
	//std::cout << message << std::endl;		
	//exit(1);
	throw std::runtime_error(message);
}


void error(std::string message1, std::string message2)
{
	throw std::runtime_error(message1 + message2);
}

int main()
{
	try
	{
		double input = some_function();
		std::cout << "input:" << input << std::endl;
	}
	catch(std::runtime_error &e)
	{
		std::cerr << "runtime error: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
